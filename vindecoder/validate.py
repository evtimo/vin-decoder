def validate(vin):
    maps = "0123456789X"
    weights = [8, 7, 6, 5, 4, 3, 2, 10, 0, 9, 8, 7, 6, 5, 4, 3, 2]
    values = {
        "0": 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9,
        "A": 1, "B": 2, "C": 3, "D": 4, "E": 5, "F": 6, "G": 7, "H": 8,
        "J": 1, "K": 2, "L": 3, "M": 4, "N": 5, "P": 7, "R": 9,
        "S": 2, "T": 3, "U": 4, "V": 5, "W": 6, "X": 7, "Y": 8, "Z": 9,
    }

    if not isinstance(vin, str) and not isinstance(vin, unicode):
        raise Exception("Input not a string!")

    if len(vin) != 17:
        raise Exception("Must be 17 symbols length!")

    vin = vin.upper()
    if "I" in vin or "O" in vin or "Q" in vin:
        raise Exception("Must not contain O/Q/I")

    total = 0
    for i, j in enumerate(vin):
        try:
            tmp = values[i] * weights[j]
        except KeyError:
            break
        total += tmp

    k = total % 11
    return maps[k] == vin[8]
